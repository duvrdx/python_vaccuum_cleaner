class Sensor(object):
    def __init__(self, initial_position: int) -> None:
        self.position: int = initial_position

    def update_position(self, new_position: int) -> None:
        self.position = new_position

class PositionSensor(Sensor):
    def __init__(self, initial_position: int) -> None:
        super().__init__(initial_position)

    def get_position(self) -> int:
        return self.position

class DustSensor(Sensor):
    def __init__(self, initial_position: int) -> None:
        super().__init__(initial_position)

    def is_dirty(self, environment_map: list[int]) -> bool:
        return environment_map[self.position] == 1

class Environment(object):
    def __init__(self, environment_map: list[int]) -> None:
        self.environment_map = environment_map

    def clean(self, position: int) -> None:
        self.environment_map[position] = 0

    def get_map(self) -> list[int]:
        return self.environment_map
      
    def is_cleaned(self) -> bool:
        return all([value == 0 for value in self.environment_map])

class Robot(object):

    ENERGY_TAKEN = {
        "move": 10,
        "clean": 20
    }

    def __init__(self, position_sensor: PositionSensor, dust_sensor: DustSensor, environment: Environment) -> None:
        self.position_sensor = position_sensor
        self.dust_sensor = dust_sensor
        self.environment = environment
        self.historical_positions = set()
        self.direction = 1
        self.energy_spent = 0

    def is_finished(self) -> bool:
        return len(self.historical_positions) == len(self.environment.get_map()) and self.environment.is_cleaned()

    def check_direction_and_update(self) -> None:
        next_position = self.position_sensor.get_position() + self.direction

        # Handle edge cases: wrap around or stay at the boundary
        if next_position < 0:
            next_position = 0
            self.direction = 1
        elif next_position >= len(self.environment.get_map()):
            next_position = len(self.environment.get_map()) - 1
            self.direction = -1

        self.position_sensor.update_position(next_position)
        self.dust_sensor.update_position(next_position)

    def move(self) -> None:
        self.check_direction_and_update()
        self.historical_positions.add(self.position_sensor.get_position())
        self.energy_spent += self.ENERGY_TAKEN["move"]

    def clean(self) -> None:
        current_position = self.position_sensor.get_position()
        print(f"Cleaning position: {current_position}")
        if self.dust_sensor.is_dirty(self.environment.get_map()):
            self.environment.clean(current_position)
            self.energy_spent += self.ENERGY_TAKEN["clean"]

        self.move()

    def clean_environment(self) -> None:
        counter = 0
        while not self.is_finished():
            print(self.environment.get_map())
            counter += 1
            self.clean()

        print(self.environment.get_map())
        print(f"Finished in {counter} steps\nEnergy spent: {self.energy_spent}")
